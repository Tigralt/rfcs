- Feature Name: server-frontend  (**Overseer**)
- Start Date: 2018-07-15
- Tracking Issue: 0003

# 1. Summary
[summary]: #summary

The server frontend feature is a graphical user interface using web technologies to manage the game server (monitoring jobs, configuration, managing plugins, ...).

# 2. Motivation
[motivation]: #motivation

The goal of of this feature is to provide a graphical user interface tool for server management purpose. Any server-side feature that can be tweaked would be managed from this graphical user interface.

# 3. Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

The server frontend, named **Overseer**, has for purpose to introduce several panels to manage the server:
- A job monitoring tool of the differents tasks running on the Veloren game server. This feature is an interface that displays the jobs currently running on the game server. It is possible to access the job details in order to get some specific statistics (e.g. uptime, resources used, ...).
- A configuration panel in order to modify the game options (e.g. network port, maximum active players, ...)
- Plugin management panel for the installation/activation of new mods, their configuration and their uninstallation/deactivation.

This feature is for the game server management only and should not have any direct relation to the game. The interaction between the game server and the server frontend should be through a graphical user interface that will be details further in this document.

# 4. Reference-level explanation
[reference-level-explanation]: #reference-level-explanation

The server frontend feature should not directly interact with any part of the main Veloren game. All tools require an interface between the server frontend and the game server in order to avoid any side effects. In this part, for each tool is described how they are interfaced with the game server and what is avalaible in the server frontent user interface.

## 4.1 Job monitoring tool

The job monitoring tool display all the game server tasks and details about each one of them.

[Diagram required]

The server frontend take place as a web server using the [Rocket](https://github.com/SergioBenitez/Rocket) crate that display a web interface. The actors are managed using the [Actix](https://github.com/actix/actix) crate. All other part of this feature are developed from scratch.

## 4.1.1 Interface

The interface between the game server and the job monitoring tool of the server frontend is a message broker service. Any game server jobs should register at start to this message broker service which is part of the server frontend. In case the server frontend is not running, the game server jobs should not try to register to the service, but if the server frontend is started while the game server jobs are already running, the game server jobs should then register to the service when the server frontend is fully up and running. The game server jobs register to this message broker service which is a part of the server frontend. This message broker feeds differents channels for each type of informations to be provided to the interface and each messages going through a channel has a payload that indicates which game server job provided it. Each channel publishes messages as soon as the message broker feeds one to the channel. For each channel, a client is acting as a subscriber and receive the channel messages. Each clients is represented as an asynchronous actor that feeds the interface of pre-processed messages about the job. Each job in the interface also is an an asynchronous actor that requires frequent updates to be considered still alive. A job actor stores all informations provided by client actors to process them as statistics. A job actor should not be erased when a client actor signal the end of the game server job's linked to it or if the job actor timeout to still get the informations linked to it.

### 4.1.2 Actors

Each game server task that registers to the game server service are represented in the job monitoring tool of the server frontend by an actor. Also, each channel of the message broker service should be an actor. So there is two types of actors in the server frontend:

- Clients 
- Jobs

#### 4.1.2.1 Clients

- A client actor is a subscriber to the message broker channels.
- A client actor upon receiving a message process it and feed it to the identified job actor.

#### 4.1.2.2 Jobs

- A job actor is a server side job overseer. 
- A job actor represents one server-side tasks, be it running or dead. 
- A job actor regroups all informations about the server-side job it oversee. 
- A job actor computes statistics with those informations.
- A job actor timeouts if there was no updates from a client actor over a predetermined time.

The informations a job actor stores and the statistics a job actor computes are in 4.2.2.

## 4.1.3 Graphical interface

The interface is split in three views:

- Job list
- Job details
- Server configuration

### 4.1.3.1 Job list

The job list view displays all jobs that are currently running or that are dead. Filters are available to be able to select what needs to be displayed and lessen the visual workload of the interface. Filters are:

- Lifecycle (Running/Dead)
- Uptime (Ascending/Descending)
- Type (To be defined)
- (More are to be defined)

The default filter is running jobs, descending uptime and any type.

### 4.1.3.2 Job details

The job details view displays all informations and statistics about a specific selected job. The avalaible informations are:

- Uptime
- Resources (RAM/CPU usage/Thread #)
- Lifecycle (Running/Dead)
- (More are to be defined)

The statistics are:

- (More are to be defined)
  
## 4.2 Configuration panel tool

The configuration panel tool displays all game server parameters that are alterable in order to provide a graphical user interface for game server configuration. Those parameters are:

- Server port
- Maximum active players
- (More are to be defined)

### 4.2.1 Interface

The interface between the game server and the configuration panel tool of the server frontend is a file. The configuration panel tool reads the file to display the configuration and writes the file to update configuration.

## 4.3 Plugin management tool

The plugin management tool manages all game server plugins.

### 4.3.1 Interface

The interface needs to be discussed when plugins will be implemented.

# 5. Drawbacks
[drawbacks]: #drawbacks

A graphical user interface requires more development and tests than a command line interface with a file oriented configuration.

# 6. Rationale and alternatives
[alternatives]: #alternatives

A command line interface will be able to do the same job, but it will not be as user friendly.

# 7. Prior art
[prior-art]: #prior-art

Any server management system.

# 8. Unresolved questions
[unresolved]: #unresolved-questions

More details about channels, informations and statistics should be discussed and approved by the contributors.

The message broker service is not decided yet and need further discussion with the contributors.